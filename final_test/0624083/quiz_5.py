import sys


def ntwominusone(n):
    n = int(n)
    return 1 << n-1


def main():
    print(ntwominusone(sys.argv[1]))


if __name__ == "__main__":
    main()