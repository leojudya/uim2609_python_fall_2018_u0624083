import pprint


def printTable(arr):
    lenx = len(arr[0])
    leny = len(arr)
    maxlen = []
    for i in range(leny):
        maxi = -1
        for j in range(lenx):
            if len(arr[i][j]) > maxi:
                maxi = len(arr[i][j])
        maxlen.append(maxi)

    for i in range(lenx):
        for j in range(leny):
            print(f"%{maxlen[j]}s" % arr[j][i]+ " ", end="")
        print()
    


def main():
    tableData = [['apples', 'oranges', 'cherries', 'banana'],
                ['Alices', 'Bob', 'Carol', 'David'],
                ['dogs', 'cats', 'mooses', 'goose']]
    printTable(tableData)

if __name__ == "__main__":
    main()