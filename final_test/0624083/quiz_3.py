import sys


def printArray(arr):
    for i in range(len(arr)):
        for j in range(len(arr[0])):
            if j == len(arr[0]):
                print(arr[i][j], end='')
            else:
                print(arr[i][j] + " ", end='')
        print()


def transformer(x, y, arr):
    x = int(x)
    y = int(y)
    l1 = []
    l2 = []
    
    q = 0
    for i in range(y):
        tmp = []
        for j in range(x):
            tmp.append(arr[q])
            q = q + 1
        l1.append(tmp)
    
    q = 0
    for i in range(x):
        tmp = []
        for j in range(y):
            tmp.append(arr[q])
            q = q + 1
        l2.append(tmp)
    
    printArray(l1)
    printArray(l2)

def main():
    transformer(sys.argv[1], sys.argv[2], sys.argv[3:])

if __name__ == "__main__":
    main()
