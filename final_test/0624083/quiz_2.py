import sys


def spam(strlist):
    result = ""
    for i in range(len(strlist)-1):
        result += strlist[i] + ', '

    result += f"and {strlist[-1]}"

    return result

def main():
    print(spam(sys.argv[1:]))


if __name__ == "__main__":
    main()
