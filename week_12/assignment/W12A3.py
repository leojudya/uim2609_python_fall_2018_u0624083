import W9A1
import sys
import uim2609_2018


def printMap(Tmplist):
    for i in Tmplist:
        for j in i:
            print(j, end=' ')
        print()


def search(Tmplist, codilist, mark):
    def dfs(x, y):
        if x < 0:
            return
        if y < 0:
            return
        if Tmplist[x][y] == mark:
            ever_list.append((x, y))
            Tmplist[x][y] = "D"
        try:
            if Tmplist[x-1][y] == mark:
                dfs(x-1, y)
        except:
            pass
        try:
            if Tmplist[x+1][y] == mark:
                dfs(x+1, y)
        except:
            pass
        try:
            if Tmplist[x][y+1] == mark:
                dfs(x, y+1)
        except:
            pass
        try:
            if Tmplist[x][y-1] == mark:
                dfs(x, y-1)
        except:
            pass

    start = codilist[0]
    ever_list = []
    dfs(codilist[0][0], codilist[0][1])
    ever_list = set(ever_list)
    codilist = set(codilist)
    return ever_list == codilist


def vertexCheck(Tmplist, x, y, mark, symbol):
    codi_list = []
    for i in range(x):
        for j in range(y):
            if Tmplist[i][j] == mark:
                codi_list.append((i, j))
    return search(Tmplist, codi_list, mark)


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    x = int(x)
    y = int(y)
    return (vertexCheck(Tmplist, x, y, mark, symbol))


if __name__ == "__main__":
    print(Mapcheck(
        W9A1.BuildMap(
            sys.argv[1],
            sys.argv[2],
            sys.argv[3],
            sys.argv[4],
            sys.argv[5]),
        sys.argv[1],
        sys.argv[2],
        sys.argv[3],
        sys.argv[4],
        sys.argv[5]))
