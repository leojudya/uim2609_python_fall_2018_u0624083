import sys


def printMatrix(l):
    for i in l:
        for j in i:
            print("%-2d" % j, end=' ')
        print()


def rotate(listtmp, m, n):
    m = int(m)
    n = int(n)
    listtmp = [int(i) for i in listtmp]
    result = []
    direction = [[0, 1], [1, 0], [0, -1], [-1, 0]]
    pos_lo = [0, -1]
    delta_index = 0
    for i in range(m):
        result.append([None] * n)
    for i in range(m*n):
        pos_lo = [
            pos_lo[0] + direction[delta_index][0],
            pos_lo[1] + direction[delta_index][1]]
        try:
            if result[pos_lo[0]][pos_lo[1]] is None and pos_lo[1] >= 0:
                result[pos_lo[0]][pos_lo[1]] = listtmp[i]
            else:
                pos_lo = [
                    pos_lo[0] - direction[delta_index][0],
                    pos_lo[1] - direction[delta_index][1]]
                delta_index = delta_index + 1
                delta_index = delta_index % 4
                pos_lo = [
                    pos_lo[0] + direction[delta_index][0],
                    pos_lo[1] + direction[delta_index][1]]
                result[pos_lo[0]][pos_lo[1]] = listtmp[i]
        except IndexError:
            pos_lo = [
                pos_lo[0] - direction[delta_index][0],
                pos_lo[1] - direction[delta_index][1]]
            delta_index = delta_index + 1
            delta_index = delta_index % 4
            pos_lo = [
                pos_lo[0] + direction[delta_index][0],
                pos_lo[1] + direction[delta_index][1]]
            result[pos_lo[0]][pos_lo[1]] = listtmp[i]
    printMatrix(result)

rotate(
    sys.argv[1:len(sys.argv)-2],
    sys.argv[-2],
    sys.argv[-1])
