import uim2609_2018
import sys


def printMatrix(l):
    for i in l:
        for j in i:
            print("%-2d" % j, end=' ')
        print()


def setTwoList(l, m, n):
    result = []
    q = 0
    for i in range(m):
        temp = []
        for j in range(n):
            temp.append(l[q])
            q = q + 1
        result.append(temp)
    return result


def roR(li, m, n):
    rl = []
    for i in range(n):
        temp = []
        for j in range(m-1, -1, -1):
            temp.append(li[j][i])
        rl.append(temp)
    return rl


def roL(li, m, n):
    rl = []
    for i in range(n-1, -1, -1):
        temp = []
        for j in range(m):
            temp.append(li[j][i])
        rl.append(temp)
    return rl


def changelist(listtemp, m, n, direct, angle):
    m = int(m)
    n = int(n)
    angle = int(angle)
    listtemp = [int(i) for i in listtemp]
    listtemp = setTwoList(listtemp, m, n)
    times = 0
    if angle % 360 == 90:
        times = 1
    elif angle % 360 == 180:
        times = 2
    elif angle % 360 == 270:
        times = 3
    if direct == 'R':
        for i in range(times):
            listtemp = roR(listtemp, m, n)
            m, n = n, m
    if direct == 'L':
        for i in range(times):
            listtemp = roL(listtemp, m, n)
            m, n = n, m
    printMatrix(listtemp)

changelist(
    sys.argv[1:len(sys.argv)-4],
    sys.argv[-4],
    sys.argv[-3],
    sys.argv[-2],
    sys.argv[-1])
