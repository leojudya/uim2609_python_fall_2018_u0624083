# _ * _ coding: utf - 8 _ * _
# Python 3 version
import uim2609_2018
import sys
import W9A1
uim2609_2018.extra['student_ID'] = 'C107118202'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
# ***


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def going(Tmplist, x, y, mark):
    if x >= 0 and y >= 0 and x < len(Tmplist) and y < len(Tmplist[0]) and \
      Tmplist[x][y] == mark:
        return True
    else:
        return False
# 判斷是否為可走點


def none_range(Tmplist, x, y, rd, mark, step):
    if step == 0:
        global stepCount
        stepCount = 0
        stop = False
        # 由左上開始搜尋橫向搜尋第一個可進入點
        for i in range(x):
            for j in range(y):
                if Tmplist[i][j] == mark:
                    '''
                    print("i:", i)
                    print("j:", j)
                    '''
                    x = i
                    y = j
                    stop = True
                    break
            if stop:
                break
        '''
        print("startX:", x)
        print("startY:", y)
        '''
        step = 1  # 因為遞迴關係，控制它只讓他第一次才會進入判斷

    # 遞迴主體
    if going(Tmplist, x, y, mark):  # 先判斷那一步可不可以走
        stepCount += 1
        Tmplist[x][y] = stepCount
        if not none_range(Tmplist, x-1, y, rd, mark, step):
            Tmplist[x][y] = mark
        elif not none_range(Tmplist, x, y-1, rd, mark, step):
            Tmplist[x][y] = mark
        elif not none_range(Tmplist, x+1, y, rd, mark, step):
            Tmplist[x][y] = mark
        elif not none_range(Tmplist, x, y+1, rd, mark, step):
            Tmplist[x][y] = mark
        else:
            if x < 0 and y < 0:
                # print("x < 0 and y < 0")
                return False
    return True


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    if x.isdigit() and y.isdigit() and \
     int(x) >= 1 and int(y) >= 1 and float(rd) <= 1 and float(rd) >= 0:
        x = int(x)
        y = int(y)
        rd = float(rd)
        symbolNum = int(float(x)*float(y)*rd)
        xy = x * y
        sumSymbol = 0
        sumMark = 0

        # 計算地圖上mark and symbol 數量
        for i in range(x):
            sumSymbol += Tmplist[i][:].count(symbol)
            sumMark += Tmplist[i][:].count(mark)
        '''
        print("sumSymbol:", sumSymbol)
        print("sumMark:", sumMark)
        print("symbolNum:", symbolNum)
        '''

        # 判斷地圖是否符合格式
        # 判斷比例是否大於或等於設定的比例
        if rd > float(symbolNum/xy):
            if sumSymbol != symbolNum+1:
                return False
            else:
                symbolNum += 1
        # 判斷橫向Mark and symbol數量
        '''
        print("Tmplist[:][x-2]:", Tmplist[:][x-2])
        print("len(Tmplist[:][x-2]):", len(Tmplist[:][x-2]))
        '''
        for i in range(x):
            if len(Tmplist[:][i]) != y:
                return False
        # 判斷縱向Mark and symbol數量
        columnNum = 0
        for i in range(y):
            for j in range(x):
                if Tmplist[j][i] is not None:
                    columnNum += 1
                # print("columnNum:", columnNum)
            if columnNum != x:
                return False
            columnNum = 0
        # 判斷Symbol and Mark 數量
        if sumSymbol != symbolNum or sumMark != xy-symbolNum:
            return False

        # 地圖顯示，檢查用
        '''
        for i in range(x):
            for j in range(y):
                print(checklist[i][j], end=" ")
            print("\n")
        '''

        # 如果比例等於1 or 0可直接判定是否為好地圖 前面有基本判斷地圖是否正確，所以才能這樣直接判斷
        if rd == 1:
            return False
        elif rd == 0:
            return True

        # 進入地圖檢查
        global step
        step = 0
        none_range(checklist, x, y, rd, mark, step)
        '''
        print(Tmplist)
        print("x:", x)
        print("y:", y)
        '''

        # 判斷是否有圍成範圍
        if stepCount != sumMark:
            # print("stepCount != sumMark")
            return False
    else:
        return False
    return result

if len(sys.argv[:]) == 6:
    x = sys.argv[1]
    y = sys.argv[2]
    rd = sys.argv[3]
    mark = sys.argv[4]
    symbol = sys.argv[5]
    global result
    result = True
    errorval = 1
    if is_number(rd) is False:
        errorval = None
else:
    errorval = None

if __name__ == '__main__':
    if errorval is None:
        print("False")
    else:
        checklist = W9A1.BuildMap(x, y, rd, mark, symbol)
        # print(checklist)
        print(Mapcheck(checklist, x, y, rd, mark, symbol))
