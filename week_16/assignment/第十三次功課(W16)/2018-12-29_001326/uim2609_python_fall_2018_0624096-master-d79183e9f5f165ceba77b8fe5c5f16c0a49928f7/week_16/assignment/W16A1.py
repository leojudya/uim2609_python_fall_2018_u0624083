import uim2609_2018
import math
import random
import sys
import W9A1
uim2609_2018.extra['student_ID'] = 'u0624096'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print


def change(row, column, mark, Tmplist):
    Tmplist[row][column] = '*' 
    if(row-1 >= 0) and Tmplist[row-1][column] == mark:  # 上
        change(row-1, column, mark, Tmplist)
    if(row+1 < x) and Tmplist[row+1][column] == mark:  # 下
        change(row+1, column, mark, Tmplist)
    if(column-1 >= 0) and Tmplist[row][column-1] == mark:  # 左
        change(row, column-1, mark, Tmplist)
    if(column+1 < y) and Tmplist[row][column+1] == mark:  # 右
        change(row, column+1, mark, Tmplist)
    return Tmplist


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    
    for i in range(x):
        for j in range(y):
            print(Tmplist[i][j], end=" ")
        print()

    row = 0
    column = 0
    for i in range(len(Tmplist)):
        for j in range(len(Tmplist[i])):
            if Tmplist[i][j] == mark:
                row = i
                column = j
    Tmplist = change(row, column, mark, Tmplist)
    count = 0  # 計算被障礙物擋住的框框
    for i in Tmplist:
        for j in i:
            if j == mark:
                count += 1
    
    if(count != 0):
        print('False')
    else:
        print('True')
    

if __name__ == '__main__':
    x = int(sys.argv[1])
    y = int(sys.argv[2])
    rd = float(sys.argv[3])
    mark = sys.argv[4]
    symbol = sys.argv[5]
    Tmplist = W9A1.BuildMap(x, y, rd, mark, symbol)
    Mapcheck(Tmplist, x, y, rd, mark, symbol)