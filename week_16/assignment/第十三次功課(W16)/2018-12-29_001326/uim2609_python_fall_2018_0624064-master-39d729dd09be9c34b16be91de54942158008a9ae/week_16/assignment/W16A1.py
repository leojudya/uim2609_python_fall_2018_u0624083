# _ * _ coding: utf - 8 _ * _
# Python 3 version
import W9A1
import sys
import uim2609_2018
uim2609_2018.extra['student_ID'] = '0624064'
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
# ***

x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]
xy = int(x*y)
rate = int(rd*xy)
# 上[x-1][y]下[x+1][y]左[y-1][x]右[y+1][x]


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    def valid(Tmplist, x, y):
        if (x >= 0 and x < len(Tmplist) and y >= 0 and y < len(Tmplist[0]) and Tmplist[x][y] == symbol):
            return True
        else:
            return False
    if valid(Tmplist, x, y):
        Tmplist[x][y] = 2
        if not valid(Tmplist, x-1, y):
            Tmplist[x][y] = symbol
        elif not valid(Tmplist, x, y-1):
            Tmplist[x][y] = symbol
        elif not valid(Tmplist, x+1, y):
            Tmplist[x][y] = symbol
        elif not valid(Tmplist, x, y+1):
            Tmplist[x][y] = symbol
        else:
            return False
    return True

if __name__ == '__main__':
    print(Mapcheck(W9A1.BuildMap(x, y, rd, mark, symbol), x, y, rd, mark, symbol))
