import numpy as np
import uim2609_2018
import W9A1
import sys
uim2609_2018.extra['student_ID'] = '0624081'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print


def change(loc, mark):
    x = loc[0]
    y = loc[1]
    maze[x][y] = '*'
    if x - 1 >= 0 and maze[x - 1][y] == mark:        # up
        change([loc[0] - 1, loc[1]], mark)
    if x + 1 < row and maze[x + 1][y] == mark:       # down
        change([loc[0] + 1, loc[1]], mark)
    if y - 1 >= 0 and maze[x][y - 1] == mark:        # left
        change([loc[0], loc[1] - 1], mark)
    if y + 1 < column and maze[x][y + 1] == mark:    # right
        change([loc[0], loc[1] + 1], mark)
    return maze


def Mapcheck(maze, x, y, mark):
    loc = [None] * 2
    for i in range(len(maze)):
        for j in range(len(maze[i])):
            if maze[i][j] == mark:
                loc[0], loc[1] = i, j
    maze = change(loc, mark)
    count = 0
    for i in maze:
        for j in i:
            if j == mark:
                count += 1
    print('False' if count != 0 else 'True')


if __name__ == '__main__':
    row = int(sys.argv[1])
    column = int(sys.argv[2])
    rd = float(sys.argv[3])
    mark = sys.argv[4]
    symbol = sys.argv[5]
    maze = W9A1.BuildMap(row, column, rd, mark, symbol)
    print(np.array(maze))
    Mapcheck(maze, row, column, mark)
