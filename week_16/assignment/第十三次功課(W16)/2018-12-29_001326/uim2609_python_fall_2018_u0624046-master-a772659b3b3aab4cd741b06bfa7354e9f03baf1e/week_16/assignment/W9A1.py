import sys
import random
import math
# _ * _ coding: utf - 8 _ * _
# Python 3 version
import uim2609_2018
uim2609_2018.extra['student_ID'] = '0624046'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
# ***


def BuildMap(x, y, rd, mark, symbol):
    all = x * y
    symbol_Q = int(math.ceil(all * rd))
    wordlist = [symbol]*symbol_Q + [mark]*(all-symbol_Q)
    random.shuffle(wordlist)
    listTmp = []
    listTmp = [[wordlist[i+(y*j)] for i in range(y)]for j in range(x)]
   
    return listTmp

x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]
if __name__ == '__main__':
    print(BuildMap(x, y, rd, mark, symbol))