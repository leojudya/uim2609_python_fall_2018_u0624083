import numpy as np
import sys
import W9A1
import uim2609_2018
uim2609_2018.extra['student_ID'] = '0624051'  
uim2609_2018.infoToSplunk()
print = uim2609_2018.print

x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]


def Mapcheck(Tmplist, x, y, mark):
    symbol = [None] * 2
    for i in range(len(Tmplist)):
        for j in range(len(Tmplist[i])):
            if Tmplist[i][j] == mark:
                symbol[0], symbol[1] = i, j
    Tmplist = change(symbol, mark)
    count = 0
    for i in Tmplist:
        for j in i:
            if j == mark:
                count += 1
    print('False' if count != 0 else 'True')


def change(symbol, mark):
    x = symbol[0]
    y = symbol[1]
    Tmplist[x][y] = '*'
    if x - 1 >= 0 and Tmplist[x - 1][y] == mark:   
        change([symbol[0] - 1, symbol[1]], mark)
    if x + 1 < x and Tmplist[x + 1][y] == mark:    
        change([symbol[0] + 1, symbol[1]], mark)
    if y - 1 >= 0 and Tmplist[x][y - 1] == mark:   
        change([symbol[0], symbol[1] - 1], mark)
    if y + 1 < y and Tmplist[x][y + 1] == mark:    
        change([symbol[0], symbol[1] + 1], mark)
    return Tmplist

if __name__ == '__main__':
    Tmplist = W9A1.BuildMap(x, y, rd, mark, symbol)
    print(np.array(Tmplist))
    Mapcheck(Tmplist, x, y, mark)
