# _ * _ coding: utf - 8 _ * _
# Python 3 version
import uim2609_2018
import sys
import random
import W9A1
import math
uim2609_2018.extra['student_ID'] = '0624037'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
# ***
sys.argv


def MapCheck(listA, x, y, rd, mark, symbol):
    di = [[0, 1], [1, 0], [0, -1], [-1, 0]]
    global ans
    mark_count, symbol_count, sx, sy = 0, 0, 0, 0
    rd = int(math.ceil(rd * x * y))
    for i in range(0, x):
        for j in range(0, y):
            print(listA[i][j], end='')
            if listA[i][j] == mark:
                mark_count += 1
                sx = i
                sy = j
            if listA[i][j] == symbol:
                symbol_count += 1
        print('')

    if (symbol_count != rd) or (mark_count != (x * y - rd)):
        ans = False

    def walk(maze, x, y, mark, symbol):
        maze[x][y] = symbol
        for i in range(0, 4):
            try:
                tx, ty = x + di[i][0], y + di[i][1]
                if maze[tx][ty] == mark and tx >= 0 and ty >= 0:
                    walk(maze, tx, ty, mark, symbol)
            except IndexError:
                print('', end='')
    walk(listA, sx, sy, mark, symbol)
    for i in range(0, len(listA)):
        if mark in listA[i]:
            ans = False
    return ans

x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]
ans = True
print(MapCheck(W9A1.BuildMap(x, y, rd, mark, symbol), x, y, rd, mark, symbol))
