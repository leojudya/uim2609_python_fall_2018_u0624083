# _ * _ coding: utf - 8 _ * _
# Python 3 version
import uim2609_2018
import sys
import W9A1
uim2609_2018.extra['student_ID'] = 'u0624006'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
# ***
sys.argv


def path(listft, Tmplist, x, y, mark):
    X = listft[0]
    Y = listft[1]
    Tmplist[X][Y] = '#'
    if(X-1 >= 0 and Tmplist[X-1][Y] == mark):  # 上
        listft[0] = X-1
        listft[1] = Y
        path(listft, Tmplist, x, y, mark)
    if(X+1 < x and Tmplist[X+1][Y] == mark):  # 下
        listft[0] = X+1
        listft[1] = Y
        path(listft, Tmplist, x, y, mark)
    if(Y-1 >= 0 and Tmplist[X][Y-1] == mark):  # 左
        listft[0] = X
        listft[1] = Y-1
        path(listft, Tmplist, x, y, mark)
    if(Y+1 < y and Tmplist[X][Y+1] == mark):  # 右
        listft[0] = X
        listft[1] = Y+1
        path(listft, Tmplist, x, y, mark)
    return Tmplist


def Mapcheck(Tmplist, x, y, mark, symbol):
    listft = [None]*2
    for i in range(x):
        for j in range(y):
            if(Tmplist[i][j] == mark):
                listft[0] = i  # 路徑的x
                listft[1] = j  # 路徑的y
    listp = path(listft, Tmplist, x, y, mark)
    count = 0
    for i in range(x):
        for j in range(y):
            if(Tmplist[i][j] == mark):
                count += 1
    if(count == 0):
        return True
    else:
        return False


if __name__ == '__main__':
    x = int(sys.argv[1])
    y = int(sys.argv[2])
    rd = float(sys.argv[3])
    mark = sys.argv[4]
    symbol = sys.argv[5]
    listans = []
    listans = W9A1.BuildMap(x, y, rd, mark, symbol)
    obs = 0  # 計算障礙物，如剛好全部都是，就由下面的if判斷
    for i in range(x):
        for j in range(y):
            if(listans[i][j] == symbol):
                obs += 1
    if(obs == x*y):
        print(False)
    else:
        print(Mapcheck(listans, x, y, mark, symbol))
    for i in range(x):
        for j in range(y):
            print(listans[i][j], end=' ')
        print()
