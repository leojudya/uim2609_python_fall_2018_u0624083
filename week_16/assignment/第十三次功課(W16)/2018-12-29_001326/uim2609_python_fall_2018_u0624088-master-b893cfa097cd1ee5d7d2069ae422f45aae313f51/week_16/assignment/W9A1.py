
import sys
import random

# _ * _ coding: utf - 8 _ * _
# Python 3 version
import uim2609_2018
uim2609_2018.extra['student_ID'] = '0624088'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
# ***


def BuildMap(x, y, rd, mark, symbol):

    listTmp = []
    listTmp = [[mark for i in range(y)] for j in range(x)]
    total = x*y*rd
    count = 1

    while(count <= total):
        a = random.randint(0, x-1)
        b = random.randint(0, y-1)
        if (listTmp[a][b] == mark):
            listTmp[a][b] = symbol
            count += 1

    # for i in range(x)
    return listTmp

x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]


if __name__ == '__main__':
    print(BuildMap(x, y, rd, mark, symbol))
