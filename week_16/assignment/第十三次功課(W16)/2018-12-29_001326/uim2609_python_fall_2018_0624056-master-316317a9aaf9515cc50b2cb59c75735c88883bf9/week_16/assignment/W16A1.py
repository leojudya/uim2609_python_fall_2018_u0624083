import W9A1
import math
import sys
import uim2609_2018
uim2609_2018.extra['student_ID'] = '0624056'
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
x, y, rd = int(sys.argv[1]), int(sys.argv[2]), float(sys.argv[3])
mark, symbol = sys.argv[4], sys.argv[5]


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    count = 1
    soulist = [[0 for j in range(y)] for i in range(x)]
    root = []
    sou = []
    # for a in Tmplist:  # 無括號、逗號顯示圖案
    #   print(' '.join(n for n in a))
    for i in range(x):
        for j in range(y):
            soulist[i][j] = count
            count += 1
    for i in range(x):
        for j in range(y):
            if Tmplist[i][j] == mark:
                if i == 0 and j == 0:  # 下右
                    if Tmplist[i+1][j] == mark:
                        soulist[i+1][j] = soulist[i][j]
                    if Tmplist[i][j+1] == mark:
                        if soulist[i][j+1] <= soulist[i][j]:
                            soulist[i][j] = soulist[i][j+1]
                        else:
                            soulist[i][j+1] = soulist[i][j]
                elif i == x-1 and j == y-1:  # 上左
                    if Tmplist[i-1][j] == mark:
                        if soulist[i-1][j] > soulist[i][j]:
                            for a in range(x):
                                for b in range(y):
                                    if soulist[a][b] == soulist[i-1][j]:
                                        soulist[a][b] = soulist[i][j]
                        else:
                            soulist[i][j] = soulist[i-1][j]
                    if Tmplist[i][j-1] == mark:
                        if soulist[i][j-1] > soulist[i][j]:
                            ch = soulist[i][j-1]
                            ore = soulist[i][j]
                            for a in range(x):
                                for b in range(y):
                                    if soulist[a][b] == ch:
                                        soulist[a][b] = ore
                        else:
                            soulist[i][j] = soulist[i][j-1]
                elif i == 0 and j == y-1:  # 下左
                    if Tmplist[i+1][j] == mark:
                        soulist[i+1][j] = soulist[i][j]
                    if Tmplist[i][j-1] == mark:
                        if soulist[i][j-1] > soulist[i][j]:
                            ch = soulist[i][j-1]
                            ore = soulist[i][j]
                            for a in range(x):
                                for b in range(y):
                                    if soulist[a][b] == ch:
                                        soulist[a][b] = ore
                        else:
                            soulist[i][j] = soulist[i][j-1]
                elif i == x-1 and j == 0:  # 上右
                    if Tmplist[i-1][j] == mark:
                        if soulist[i-1][j] > soulist[i][j]:
                            for a in range(x):
                                for b in range(y):
                                    if soulist[a][b] == soulist[i-1][j]:
                                        soulist[a][b] = soulist[i][j]
                        else:
                            soulist[i][j] = soulist[i-1][j]
                    if Tmplist[i][j+1] == mark:
                        if soulist[i][j+1] <= soulist[i][j]:
                            soulist[i][j] = soulist[i][j+1]
                        else:
                            soulist[i][j+1] = soulist[i][j]
                elif i == 0 and (j != y-1 and j != 0):  # 下右左
                    if Tmplist[i+1][j] == mark:
                        soulist[i+1][j] = soulist[i][j]
                    if Tmplist[i][j+1] == mark:
                        if soulist[i][j+1] <= soulist[i][j]:
                            soulist[i][j] = soulist[i][j+1]
                        else:
                            soulist[i][j+1] = soulist[i][j]
                    if Tmplist[i][j-1] == mark:
                        if soulist[i][j-1] > soulist[i][j]:
                            ch = soulist[i][j-1]
                            ore = soulist[i][j]
                            for a in range(x):
                                for b in range(y):
                                    if soulist[a][b] == ch:
                                        soulist[a][b] = ore
                        else:
                            soulist[i][j] = soulist[i][j-1]
                elif i == x-1 and (j != y-1 and j != 0):  # 上右左
                    if Tmplist[i-1][j] == mark:
                        if soulist[i-1][j] > soulist[i][j]:
                            for a in range(x):
                                for b in range(y):
                                    if soulist[a][b] == soulist[i-1][j]:
                                        soulist[a][b] = soulist[i][j]
                        else:
                            soulist[i][j] = soulist[i-1][j]
                    if Tmplist[i][j+1] == mark:
                        if soulist[i][j+1] <= soulist[i][j]:
                            soulist[i][j] = soulist[i][j+1]
                        else:
                            soulist[i][j+1] = soulist[i][j]
                    if Tmplist[i][j-1] == mark:
                        if soulist[i][j-1] > soulist[i][j]:
                            ch = soulist[i][j-1]
                            ore = soulist[i][j]
                            for a in range(x):
                                for b in range(y):
                                    if soulist[a][b] == ch:
                                        soulist[a][b] = ore
                        else:
                            soulist[i][j] = soulist[i][j-1]
                elif j == 0 and(i != 0 and i != x-1):  # 上下右
                    if Tmplist[i-1][j] == mark:
                        if soulist[i-1][j] > soulist[i][j]:
                            for a in range(x):
                                for b in range(y):
                                    if soulist[a][b] == soulist[i-1][j]:
                                        soulist[a][b] = soulist[i][j]
                        else:
                            soulist[i][j] = soulist[i-1][j]
                    if Tmplist[i+1][j] == mark:
                        soulist[i+1][j] = soulist[i][j]
                    if Tmplist[i][j+1] == mark:
                        if soulist[i][j+1] <= soulist[i][j]:
                            soulist[i][j] = soulist[i][j+1]
                        else:
                            soulist[i][j+1] = soulist[i][j]
                elif j == y-1 and (i != 0 and i != x-1):  # 上下左
                    if Tmplist[i-1][j] == mark:
                        if soulist[i-1][j] > soulist[i][j]:
                            for a in range(x):
                                for b in range(y):
                                    if soulist[a][b] == soulist[i-1][j]:
                                        soulist[a][b] = soulist[i][j]
                        else:
                            soulist[i][j] = soulist[i-1][j]
                    if Tmplist[i+1][j] == mark:
                        soulist[i+1][j] = soulist[i][j]
                    if Tmplist[i][j-1] == mark:
                        if soulist[i][j-1] > soulist[i][j]:
                            ch = soulist[i][j-1]
                            ore = soulist[i][j]
                            for a in range(x):
                                for b in range(y):
                                    if soulist[a][b] == ch:
                                        soulist[a][b] = ore
                        else:
                            soulist[i][j] = soulist[i][j-1]
                else:  # 中
                    if Tmplist[i-1][j] == mark:
                        if soulist[i-1][j] > soulist[i][j]:
                            for a in range(x):
                                for b in range(y):
                                    if soulist[a][b] == soulist[i-1][j]:
                                        soulist[a][b] = soulist[i][j]
                        else:
                            soulist[i][j] = soulist[i-1][j]
                    #################################################
                    if Tmplist[i+1][j] == mark:
                        soulist[i+1][j] = soulist[i][j]
                    #################################################
                    if Tmplist[i][j+1] == mark:
                        if soulist[i][j+1] < soulist[i][j]:
                            soulist[i][j] = soulist[i][j+1]
                        else:
                            soulist[i][j+1] = soulist[i][j]
                    #################################################
                    if Tmplist[i][j-1] == mark:
                        if soulist[i][j-1] > soulist[i][j]:
                            ch = soulist[i][j-1]
                            ore = soulist[i][j]
                            for a in range(x):
                                for b in range(y):
                                    if soulist[a][b] == ch:
                                        soulist[a][b] = ore
                        else:
                            soulist[i][j] = soulist[i][j-1]
            else:
                root.append(count)
    for i in range(x):
        for j in range(y):
            sou.append(soulist[i][j])
    if len(set(sou)) == math.ceil(x*y*rd)+1:
        return True
    else:
        return False


print(Mapcheck(W9A1.BuildMap(x, y, rd, mark, symbol), x, y, rd, mark, symbol))
