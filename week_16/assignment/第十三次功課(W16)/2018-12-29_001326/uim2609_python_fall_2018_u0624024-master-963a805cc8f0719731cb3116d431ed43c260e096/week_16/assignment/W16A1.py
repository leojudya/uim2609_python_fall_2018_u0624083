import uim2609_2018
import W9A1
import sys

uim2609_2018.extra['student_ID'] = '0624024'
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
checklist = []


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    counter = 0
    for i in range(x):
        for j in range(y):
            checklist[i][j] = counter
            counter += 1

    for i in range(x):
        for j in range(y):
            if(Tmplist[i][j] == mark):
                oriValue = checklist[i][j]
                CheckTree(Tmplist, i - 1, j, mark, symbol, checklist, oriValue)
                CheckTree(Tmplist, i, j - 1, mark, symbol, checklist, oriValue)
                CheckTree(Tmplist, i + 1, j, mark, symbol, checklist, oriValue)
                CheckTree(Tmplist, i, j + 1, mark, symbol, checklist, oriValue)

    checkTorF = []
    for i in range(x):
        for j in range(y):
            if(checklist[i][j] not in checkTorF):
                checkTorF.append(checklist[i][j])

    total = int(x * y * rd)
    if (total / (x * y) < rd):
        total += 1

    if(len(checkTorF) > total + 1):
        return False
    else:
        return True


def CheckTree(Tmplist, ix, iy, mark, symbol, checklist, oriValue):
    if((ix >= len(Tmplist)) or (iy >= len(Tmplist[0]))):
        return True
    elif((ix < 0) or (iy < 0)):
        return True
    elif(Tmplist[ix][iy] == mark):
        smallValue = oriValue
        bigValue = checklist[ix][iy]
        if(smallValue > bigValue):
            temp = smallValue
            smallValue = bigValue
            bigValue = temp
        for i in range(len(checklist)):
            for j in range(len(checklist[i])):
                if(checklist[i][j] == bigValue):
                    checklist[i][j] = smallValue
    elif(Tmplist[ix][iy] == symbol):
        return True
    return True


x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]
checklist = [[None] * y for i in range(x)]
print(Mapcheck(W9A1.BuildMap(x, y, rd, mark, symbol), x, y, rd, mark, symbol))
