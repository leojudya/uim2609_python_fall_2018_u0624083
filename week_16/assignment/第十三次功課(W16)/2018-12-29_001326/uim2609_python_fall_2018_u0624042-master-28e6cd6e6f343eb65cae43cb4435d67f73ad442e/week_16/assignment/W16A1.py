import sys
import uim2609_2018
import random
import W9A1
sys.argv
uim2609_2018.extra['student_ID'] = '0624042'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print


def BuildMap(x, y, rd, mark, symbol):
    did_times = 0
    need_times = x * y * rd
    listTmp = [[mark for i in range(y)] for j in range(x)]
    while did_times < need_times:
        a = random.randint(0, x-1)
        b = random.randint(0, y-1)
        if(listTmp[a][b] == mark):
            listTmp[a][b] = symbol
            did_times = did_times + 1
    return listTmp


def rotate(Original_list):
    Result_list = list(zip(*Original_list[::-1]))
    Result_list = [list(item) for item in Result_list]
    return Result_list


def realMapcheck(Tmplist, x, y, mark, symbol):

    x = len(Tmplist)
    y = len(Tmplist[0])

    for i in range(x):
        for j in range(y):

            if(j+1 < y):
                if(Tmplist[i][j] < Tmplist[i][j+1] and Tmplist[i][j] != 0):
                    Tmplist[i][j+1] = Tmplist[i][j]

            if(i+1 < x):
                if(Tmplist[i][j] < Tmplist[i+1][j] and Tmplist[i][j] != 0):
                    Tmplist[i+1][j] = Tmplist[i][j]
    return Tmplist


def Mapcheck(Tmplist, x, y, mark, symbol):
    checkFirst = False
    for i in range(len(Tmplist)):
        print(Tmplist[i])

    listA = [[0 for i in range(y)] for j in range(x)]   
    count = 1
    for i in range(x):
        for j in range(y):
            listA[i][j] = count
            count += 1
    # print(listA)
    # print("-------")

    for i in range(x):
        for j in range(y):

            if(Tmplist[i][j] == symbol):
                listA[i][j] = 0

    # print(listA)
    # print("-------")

    for i in range(4):
        if(i == 0):
            if(listA[0][0] == 0):
                # print("CHECKED")
                listA[0][0] = 1
                checkFirst = True

        listA = realMapcheck(listA, x, y, mark, symbol)

        listA = rotate(listA)

        if(checkFirst):
            listA[0][0] == 0
            checkFirst = False

    listB = []
    for i in range(x):
        for j in range(y):
            listB.append(listA[i][j])
    # print(max(listB))

    if (max(listB) > 1):
        return False
    else:
        return True


x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]

print(Mapcheck(BuildMap(x, y, rd, mark, symbol), x, y, mark, symbol))
