import sys
import W9A1
import uim2609_2018
uim2609_2018.extra['student_ID'] = '0624030'
uim2609_2018.infoToSplunk()
print = uim2609_2018.print


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    total = x*y*rd
    if total//1 < total:
        total = int(total)+1
    ratio = x * y - total
    for i in range(y):
        for j in range(x):
            if Tmplist[i][j] == mark:
                routeHistory = [[i, j]]
                break
        if len(routeHistory) != 0:
            break
    rightroute = [routeHistory[-1]]
    location = routeHistory[-1]
    while len(rightroute) != 0:
        if goup(location, routeHistory, Tmplist, rightroute):
            location = rightroute[-1]
            continue
        if godown(location, routeHistory, Tmplist, rightroute):
            location = rightroute[-1]
            continue
        if goleft(location, routeHistory, Tmplist, rightroute):
            location = rightroute[-1]
            continue
        if goright(location, routeHistory, Tmplist, rightroute):
            location = rightroute[-1]
            continue
        rightroute.pop()
        if len(rightroute) != 0:
            location = rightroute[-1]
    if len(routeHistory) < ratio:
        return False
    else:
        return True


def goup(location, routeHistory, Tmplist, rightroute):
    if location[0] == 0:
        return False
    else:
        newlocation = [location[0] - 1, location[1]]
        if newlocation in routeHistory:
            return False
        elif Tmplist[newlocation[0]][newlocation[1]] == symbol:
            return False
        else:
            routeHistory.append(newlocation)
            rightroute.append(newlocation)
            return True


def godown(location, routeHistory, Tmplist, rightroute):
    if location[0] == y - 1:
        return False
    else:
        newlocation = [location[0] + 1, location[1]]
        if newlocation in routeHistory:
            return False
        elif Tmplist[newlocation[0]][newlocation[1]] == symbol:
            return False
        else:
            routeHistory.append(newlocation)
            rightroute.append(newlocation)
            return True


def goleft(location, routeHistory, Tmplist, rightroute):
    if location[1] == 0:
        return False
    else:
        newlocation = [location[0], location[1] - 1]
        if newlocation in routeHistory:
            return False
        elif Tmplist[newlocation[0]][newlocation[1]] == symbol:
            return False
        else:
            rightroute.append(newlocation)
            routeHistory.append(newlocation)
            return True


def goright(location, routeHistory, Tmplist, rightroute):
    if location[1] == x - 1:
        return False
    else:
        newlocation = [location[0], location[1] + 1]
        if newlocation in routeHistory:
            return False
        elif Tmplist[newlocation[0]][newlocation[1]] == symbol:
            return False
        else:
            routeHistory.append(newlocation)
            rightroute.append(newlocation)
            return True

x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]
print(Mapcheck(W9A1.BuildMap(x, y, rd, mark, symbol), x, y, rd, mark, symbol))
