import uim2609_2018
import random
import sys
uim2609_2018.extra['student_ID'] = '0624103'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print

sys.argv
x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]


def BuildMap(x, y, rd, mark, symbol):
    c = 0
    listTmp = []
    listTmp = [[symbol for i in range(y)] for j in range(x)]
    rdc = int((x * y) * rd+0.5)
    if ((x * y) * rd) > rdc:
        rdc = rdc+1
    while c != rdc:
        if c == rdc:
            break
        c = 0
        for i in range(x):
            for j in range(y):
                rd1 = random.random()
                if rd1 > rd:
                    listTmp[i][j] = mark
                elif rd1 <= rd:
                    listTmp[i][j] = symbol
                    c = c + 1

    return listTmp

if __name__ == '__main__':
    print(BuildMap(x, y, rd, mark, symbol))
