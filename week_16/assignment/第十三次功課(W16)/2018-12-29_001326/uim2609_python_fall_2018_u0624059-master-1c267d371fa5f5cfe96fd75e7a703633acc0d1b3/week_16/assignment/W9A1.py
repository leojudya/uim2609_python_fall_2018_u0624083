import sys
import random
import math

# _ * _ coding: utf - 8 _ * _
# Python 3 version
import uim2609_2018
uim2609_2018.extra['student_ID'] = 'u0624059'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
# ***


def BuildMap(x, y, rd, mark, symbol):
    listTmp = []
    list1 = [mark, symbol]
    list2 = ((sys.argv[4])*((x*y)-(math.ceil((x*y)*rd))) +
             (sys.argv[5])*(math.ceil((x*y)*rd)))
    listTmp = list(list2)
    random.shuffle(listTmp)
    listTmp = [listTmp[i:i+y] for i in range(0, len(listTmp), y)]

    return listTmp


x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]
if __name__ == '__main__':
    print(BuildMap(x, y, rd, mark, symbol))
