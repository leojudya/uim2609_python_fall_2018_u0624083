import uim2609_2018
uim2609_2018.extra['student_ID'] = '0624063'
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
import sys
import W9A1

x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]


def Mapcheck(listTmp, x, y, rd, mark, symbol):
    def dfs(start_x, start_y, end_x, end_y, list1):
        global number
        next_step = [[0, 1], [1, 0], [0, -1], [-1, 0]]
        for i in range(len(next_step)):
            next_x = start_x + next_step[i][0]
            next_y = start_y + next_step[i][1]
            if(next_x < 0 or next_y < 0):
                continue
            if(next_x > len(list1)-1 or next_y > len(list1[0])-1):
                continue
            elif(array1[next_x][next_y] == 0 and array2[next_x][next_y] == 0):
                array2[next_x][next_y] = 1
                number += 1
                dfs(next_x, next_y, end_x, end_y, list1)
    dfs(start_x, start_y, end_x, end_y, list1)
    if(number == marknumber):
        return True
    else:
        return False

if __name__ == '__main__':
    number = 1
    marknumber = 0
    array1 = [[0 for i in range(y)] for j in range(x)]
    array2 = [[0 for i in range(y)] for j in range(x)]
    list1 = W9A1.BuildMap(x, y, rd, mark, symbol)

    for i in range(len(list1)):
        for j in range(len(list1[i])):
            if(list1[i][j] == mark):
                start_x = i
                start_y = j
                break
        if(list1[i][j] == mark):
            break

    for i in range(len(list1)-1, -1, -1):
        for j in range(len(list1[i])-1, -1, -1):
            if(list1[i][j] == mark):
                end_x = i
                end_y = j
                break
        if(list1[i][j] == mark):
            break
    for i in range(len(list1)):
        for j in range(len(list1[i])):
            if(list1[i][j] == mark):
                marknumber += 1
                list1[i][j] = 0
            else:
                list1[i][j] = 1
    for i in range(len(list1)):
        for j in range(len(list1[i])):
            array1[i][j] = list1[i][j]
    array2[start_x][start_y] = 1
    print(
        Mapcheck(W9A1.BuildMap(x, y, rd, mark, symbol), x, y, rd, mark, symbol)
        )
