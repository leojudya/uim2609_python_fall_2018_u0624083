# _ * _ coding: utf - 8 _ * _
# Python 3 version
import uim2609_2018
import W9A1
import sys
uim2609_2018.extra['student_ID'] = '0624011'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
# ***

global endX, endY, Map, findIt, passed
sys.setrecursionlimit(30000)


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    global endX, endY, Map, findIt, passed

    Map = [[1 for i in range(y+1)]for j in range(x+1)]
    endX = -1
    findLoop = True

    for i in range(len(Tmplist)):   # 設定終點及格式化
        for j in range(len(Tmplist[i])):
            if(Tmplist[i][j] == mark):
                Map[i][j] = 0
                endX = j
                endY = i
    # for i in Map:
    #     for j in i:
    #         print(j, end=" ")
    #     print()

    if(endX != -1):
        for i in range(len(Map)):     # 開始跑
            for j in range(len(Map[i])):
                if(Map[i][j] == 0):
                    findIt = False
                    passed = []
                    Walk(j, i)
                    if(not findIt):
                        findLoop = False
                        break
            if(not findLoop):
                break
    else:
        findLoop = False

    return findLoop


def Walk(x, y):
    global Map, endX, endY, findIt, passed
    tmpW = str(x) + "," + str(y)
    if(not findIt and tmpW not in passed):
        if(Map[y][x] == 0):
            Map[y][x] = 1
            if(x == endX and y == endY):
                findIt = True
            # print("------------------")
            # for i in Map:
            #     for j in i:
            #         print(j, end=" ")
            #     print()

            if(x-1 >= 0 and y >= 0):
                # print("L")
                Walk(x-1, y)     # 左

            if(x >= 0 and y-1 >= 0):
                # print("Up")
                Walk(x, y-1)     # 上

            if(x+1 >= 0 and y >= 0):
                # print("R")
                Walk(x+1, y)     # 右

            if(x >= 0 and y+1 >= 0):
                # print("Down")
                Walk(x, y+1)     # 下

            Map[y][x] = 0
            passed.append(str(x) + "," + str(y))

if __name__ == '__main__':
    if(len(sys.argv) == 6):
        x = int(sys.argv[1])
        y = int(sys.argv[2])
        rd = float(sys.argv[3])
        mark = sys.argv[4]
        symbol = sys.argv[5]

        print(str(Mapcheck(W9A1.BuildMap(
            x, y, rd, mark, symbol), x, y, rd, mark, symbol)))

    else:
        print("請檢查輸入內容")
