# _ * _ coding: utf - 8 _ * _
# Python 3 version
import uim2609_2018
import sys
import random
uim2609_2018.extra['student_ID'] = '0624052'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
# ***


def BuildMap(x, y, rd, mark, symbol):
    prinum = 0
    listTmp = [[mark for i in range(y)] for j in range(x)]
    itemnum = int(x * y * rd)
    if((itemnum / (x * y)) < rd):
        itemnum += 1
    while(prinum < itemnum):
        xr = random.randint(0, x - 1)
        yr = random.randint(0, y - 1)
        if(listTmp[xr][yr] is not symbol):
            # print('(%d, %d)' % (xr, yr))
            listTmp[xr][yr] = symbol
            prinum += 1
    return listTmp


def CheckMap(listTmp, x, y, rd, symbol, mark):
    itemnum = 0
    for i in range(0, len(listTmp)):
        for j in range(0, len(listTmp[i])):
            if(listTmp[i][j] == symbol):
                itemnum += 1
    if((itemnum / (x * y)) < rd):
        result = False
    else:
        result = True
    return result


if __name__ == '__main__':
    x = int(sys.argv[1])
    y = int(sys.argv[2])
    rd = float(sys.argv[3])
    mark = sys.argv[4]
    symbol = sys.argv[5]
    print(BuildMap(x, y, rd, mark, symbol))
