import sys
import W9A1
import uim2609_2018
uim2609_2018.extra['student_ID'] = '0624020'
uim2609_2018.infoToSplunk()
print = uim2609_2018.print

x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    total = x * y * rd
    if total // 1 < total:
        total = int(total) + 1
    ratio = x * y - total
    for i in range(y):
        for j in range(x):
            routeHistory = [[i, j]]
            break
        break
    
    rightRoute = [routeHistory[-1]]
    location = routeHistory[-1]
    while len(rightRoute) != 0:
        if goUp(location, routeHistory, Tmplist, rightRoute):
            location = rightRoute[-1]
            continue
        if goDown(location, routeHistory, Tmplist, rightRoute):
            location = rightRoute[-1]
            continue
        if goLeft(location, routeHistory, Tmplist, rightRoute):
            location = rightRoute[-1]
            continue
        if goRight(location, routeHistory, Tmplist, rightRoute):
            location = rightRoute[-1]
            continue
        rightRoute.pop()
        if len(routeHistory) < ratio:
            return False
        else:
            return True


def goUp(location, routeHistory, Tmplist, rightRoute):
    if(location[0] == 0):
        return False
    else:
        newLocation = [location[0] - 1, location[1]]
        if newLocation in routeHistory:
            return False
        elif Tmplist[newLocation[0]][newLocation[1]] == symbol:
            return False
        else:
            rightRoute.append(newLocation)
            routeHistory.append(newLocation)
            return True


def goDown(location, routeHistory, Tmplist, rightRoute):
    if(location[0] == y - 1):
        return False
    else:
        newLocation = [location[0] + 1, location[1]]
        if newLocation in routeHistory:
            return False       
        elif Tmplist[newLocation[0]][newLocation[1]] == symbol:
            return False
        else:
            rightRoute.append(newLocation)
            routeHistory.append(newLocation)
            return True


def goLeft(location, routeHistory, Tmplist, rightRoute):
    if(location[1] == 0):
        return False
    else:
        newLocation = [location[0], location[1] - 1]
        if newLocation in routeHistory:
            return False       
        elif Tmplist[newLocation[0]][newLocation[1]] == symbol:
            return False
        else:
            rightRoute.append(newLocation)
            routeHistory.append(newLocation)
            return True
        

def goRight(location, routeHistory, Tmplist, rightRoute):
    if(location[1] == x - 1):
        return False
    else:
        newLocation = [location[0], location[1] + 1]
        if newLocation in routeHistory:
            return False       
        elif Tmplist[newLocation[0]][newLocation[1]] == symbol:
            return False
        else:
            rightRoute.append(newLocation)
            routeHistory.append(newLocation)
            return True


print(Mapcheck(W9A1.BuildMap(x, y, rd, mark, symbol), x, y, rd, mark, symbol))
