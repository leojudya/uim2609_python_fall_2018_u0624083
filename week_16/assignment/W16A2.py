# coding=utf-8
# import uim2609_2018
import shutil
import os


def main():
    os.chdir(r'第十三次功課(W16)')
    os.mkdir('student')
    os.chdir('2018-12-29_001326')
    folder = os.listdir(os.getcwd())
    stuid = []
    for name in folder:
        stuid.append(name.split('_')[4].split('-')[0])
    os.chdir('..')
    os.chdir('student')
    for sid in stuid:
        os.mkdir(sid)

if __name__ == "__main__":
    main()
