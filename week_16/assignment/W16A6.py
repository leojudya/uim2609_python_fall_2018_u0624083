# coding=utf-8
# import uim2609_2018
import os
import shutil


def main():
    dstpath = os.getcwd() + '/第十三次功課(W16)/work/'
    os.chdir(r'第十三次功課(W16)')
    os.mkdir('work')
    os.chdir('student')
    foldername = os.listdir(os.getcwd())
    for fn in foldername:
        os.chdir(fn)
        if os.path.exists('W16A1.py'):
            shutil.copyfile('W16A1.py', dstpath + fn + '.py')
        os.chdir('..')

if __name__ == "__main__":
    main()
