# coding=utf-8
# import uim2609_2018
import os
import shutil


def main():
    dstpath = os.getcwd() + '/第十三次功課(W16)/student/'
    os.chdir(r'第十三次功課(W16)')
    os.chdir('2018-12-29_001326')
    folder = os.listdir(os.getcwd())
    stuid = []
    for name in folder:
        os.chdir(name)
        if(os.path.exists('week_16')):
            os.chdir('week_16')
            os.chdir('assignment')
            sid = name.split('_')[4].split('-')[0]
            shutil.copy('W16A1.py', dstpath + sid)
            os.chdir('..')
            os.chdir('..')
            os.chdir('..')
        else:
            os.chdir('..')


if __name__ == "__main__":
    main()
