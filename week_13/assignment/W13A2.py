import sys
# import uim2609_2018


def printList(l):
    for i in l:
        print(i, end=' ')
    print()


def to2dArray(l, m, n):
    q = 0
    re = []
    for i in range(m):
        temp = []
        for j in range(n):
            temp.append(l[q])
            q = q + 1
        re.append(temp)
    return re


def rotate_2(m, n, listtmp):
    m = int(m)
    n = int(n)
    listtmp = to2dArray(list(map(int, listtmp)), m, n)
    start = 0
    result = []

    def setNumbers():
        endX = n - 1 - start
        endY = m - 1 - start
        for i in range(start, endX + 1):
            result.append(listtmp[start][i])

        if (endY > start):
            for i in range(start + 1, endY + 1):
                result.append(listtmp[i][endX])

        if(endY > start and endX > start):
            for i in range(endX - 1, start - 1, -1):
                result.append(listtmp[endY][i])

        if(endY > start and endX > start):
            for i in range(endY - 1, start, -1):
                result.append(listtmp[i][start])

    while(start * 2 < m and start * 2 < n):
        setNumbers()
        start = start + 1

    printList(result)
if __name__ == "__main__":
    rotate_2(sys.argv[1], sys.argv[2], sys.argv[3:])
