import W9A1
import sys
# import uim2609_2018


def printMap(Tmplist):
    for i in Tmplist:
        for j in i:
            print(j, end=' ')
        print()


def search(Tmplist, codilist, mark):

    def check(x, y):
        if Tmplist[x][y] == mark:
            ever_list.append((x, y))
            Tmplist[x][y] = "D"

        if x-1 >= 0:
            if Tmplist[x-1][y] == mark:
                check(x-1, y)
        if x + 1 < len(Tmplist):
            if Tmplist[x+1][y] == mark:
                check(x+1, y)
        if y + 1 < len(Tmplist[0]):
            if Tmplist[x][y+1] == mark:
                check(x, y+1)
        if y - 1 >= 0:
            if Tmplist[x][y-1] == mark:
                check(x, y-1)

    start = codilist[0]
    ever_list = []
    check(codilist[0][0], codilist[0][1])
    ever_list = set(ever_list)
    codilist = set(codilist)
    return ever_list == codilist


def vertexCheck(Tmplist, x, y, mark, symbol):
    codi_list = []
    for i in range(x):
        for j in range(y):
            if Tmplist[i][j] == mark:
                codi_list.append((i, j))
    return search(Tmplist, codi_list, mark)


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    x = int(x)
    y = int(y)
    return (vertexCheck(Tmplist, x, y, mark, symbol))


if __name__ == "__main__":
    print(Mapcheck(
        W9A1.BuildMap(
            sys.argv[1],
            sys.argv[2],
            sys.argv[3],
            sys.argv[4],
            sys.argv[5]),
        sys.argv[1],
        sys.argv[2],
        sys.argv[3],
        sys.argv[4],
        sys.argv[5]))
