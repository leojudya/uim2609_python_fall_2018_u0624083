import sys
import uim2609_2018


def printMatrix(ma):
    for i in ma:
        for j in i:
            print(j, end='\t')
        print()


def rotate(listtmp, m, n):
    listtmp = list(map(int, listtmp))
    m = int(m)
    n = int(n)
    result = []
    start = 0

    for i in range(m):
        temp = [None] * n
        result.append(temp)

    def setNumbers():
        endX = n - 1 - start
        endY = m - 1 - start
        for i in range(start, endX + 1):
            result[start][i] = listtmp.pop(0)

        if (endY > start):
            for i in range(start + 1, endY + 1):
                result[i][endX] = listtmp.pop(0)

        if(endY > start and endX > start):
            for i in range(endX - 1, start - 1, -1):
                result[endY][i] = listtmp.pop(0)

        if(endY > start and endX > start):
            for i in range(endY - 1, start, -1):
                result[i][start] = listtmp.pop(0)
    while(start * 2 < m and start * 2 < n):
        setNumbers()
        start = start + 1

    printMatrix(result)


if __name__ == "__main__":
    rotate(sys.argv[1:-2], sys.argv[-2], sys.argv[-1])
