import sys
import uim2609_2018


def recursive_2(n):
    if n == 1:
        return 1
    else:
        return recursive_2(n-1) * 2


def sdot(n):
    n = str(n)
    return (f"{n[0]}.{n[1:2]}E+{len(n[1:])}")


print(sdot(recursive_2(int(sys.argv[1]))))
