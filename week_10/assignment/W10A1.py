import sys
import uim2609_2018


def recursive_1(n, prev=0, val=1):
    if n == -1:
        return prev
    if n == 0:
        return val
    return recursive_1(n - 1, val, prev + val)


def sdot(n):
    n = str(n)
    return (f"{n[0]}.{n[1:2]}E+{len(n[1:])}")


sys.setrecursionlimit(5000)
print(sdot(recursive_1(int(sys.argv[1]))))
