# import uim2609_2018
import sys
import random
import math


def BuildMap(x, y, rd, mark, symbol):
    x = int(x)
    y = int(y)
    rd = float(rd)
    listTmp = []
    sym_count = 0
    if (math.floor((x*y*rd)) / (x*y) >= rd):
        sym_count = math.floor((x*y*rd))
    else:
        sym_count = math.ceil((x*y*rd))
    blank_count = x * y - sym_count
    sym_list = [symbol for i in range(sym_count)]
    for i in range(blank_count):
        sym_list.append(mark)
    sym_list = random.sample(sym_list, len(sym_list))
    q = 0
    for i in range(y):
        temp = []
        for j in range(x):
            temp.append(sym_list[q])
            q = q + 1
        listTmp.append(temp)
    return listTmp


if __name__ == '__main__':
    x = int(sys.argv[1])
    y = int(sys.argv[2])
    rd = float(sys.argv[3])
    mark = sys.argv[4]
    symbol = sys.argv[5]
    print(BuildMap(x, y, rd, mark, symbol))
