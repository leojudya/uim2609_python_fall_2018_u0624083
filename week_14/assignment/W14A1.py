#  import uim2609_2018
import sys


def HN(n, p1, p2, p3):
    if n == 1:
        print(f'{p1} to {p3}')
    else:
        HN(n-1, p1, p3, p2)
        HN(1, p1, p2, p3)
        HN(n-1, p2, p1, p3)


HN(int(sys.argv[1]), sys.argv[2], sys.argv[3], sys.argv[4])
