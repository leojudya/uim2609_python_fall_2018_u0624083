import W9A1
import sys
# import uim2609_2018


def search(Tmplist, codilist, mark, x, y):
    def dfs(graph, start):
        visited, stack = set(), [start]
        while stack:
            vertex = stack.pop()
            if vertex not in visited:
                visited.add(vertex)
                stack.extend(graph[vertex] - visited)
        return visited

    start = codilist[0]
    graph = createGraph(codilist,  x, y, mark)
    visited = dfs(graph, start)
    codilist = set(codilist)
    return visited == codilist


def vertexCheck(Tmplist, x, y, mark, symbol):
    codi_list = []
    for i in range(y):
        for j in range(x):
            if Tmplist[i][j] == mark:
                codi_list.append((i, j))
    return search(Tmplist, codi_list, mark, x, y)


def createGraph(codilist, x, y, mark):
    graph = {}
    for i in range(y):
        for j in range(x):
            tmp = []
            if (i-1, j) in codilist:
                tmp.append((i-1, j))
            if (i+1, j) in codilist:
                tmp.append((i+1, j))
            if (i, j+1) in codilist:
                tmp.append((i, j+1))
            if (i, j-1) in codilist:
                tmp.append((i, j-1))
            graph[(i, j)] = set(tmp)
            tmp = []

    return graph


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    x = int(x)
    y = int(y)
    #  printMap(Tmplist)
    return (vertexCheck(Tmplist, x, y, mark, symbol))


if __name__ == "__main__":
    print(Mapcheck(
        W9A1.BuildMap(
            sys.argv[1],
            sys.argv[2],
            sys.argv[3],
            sys.argv[4],
            sys.argv[5]),
        sys.argv[1],
        sys.argv[2],
        sys.argv[3],
        sys.argv[4],
        sys.argv[5]))
