import uim2609_2018
import sys


def bigOone(n):
    return int('1' + '0' * (n-1), 2)

print(bigOone(int(sys.argv[1])))
