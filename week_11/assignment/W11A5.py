import sys
import uim2609_2018


def matrix_mul(x, y):
    res = [[0] * len(y[0]) for i in range(len(x))]
    for i in range(len(x)):
        for j in range(len(y[0])):
            for k in range(len(y)):
                res[i][j] += x[i][k] * y[k][j]
    return res


def fibonacci(n):
    q = [[1, 1], [1, 0]]
    r = [[1, 1], [1, 0]]
    for i in range(n-1):
        r = matrix_mul(r, q)
    return r[0][0]

print(fibonacci(int(sys.argv[1])))
