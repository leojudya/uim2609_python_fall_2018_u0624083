import uim2609_2018
import sys


def bigOn(n):
    n = int(n)
    q = 1
    for i in range(n-1):
        q = q * 2
    return q


if __name__ == "__main__":
    print(bigOn(int(sys.argv[1])))
