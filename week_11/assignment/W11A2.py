import uim2609_2018
import sys
import W11A1


def bigOhalfn(n):
    if n == 1:
        return 1,
    elif n == 2:
        return 2
    elif n % 2 == 0:
        return W11A1.bigOn(n/2) * W11A1.bigOn(n/2) * 2
    else:
        return W11A1.bigOn((n-1)/2 + 1) * W11A1.bigOn((n-1)/2 + 1)

print(bigOhalfn(int(sys.argv[1])))
