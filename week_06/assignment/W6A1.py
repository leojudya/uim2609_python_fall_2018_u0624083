import uim2609_2018


def check_valid(txt):
    for char in txt:
        if (char.isdigit() or char.islower() or char.isupper()):  # 檢查是否英文字母或數字
            continue
        else:
            return False
    return True


def convert_to_result(txt):
    changed_txt = ""
    for char in txt:
        if (char.islower()):
            changed_txt += char.upper()  # 小轉大
        elif (char.isupper()):
            changed_txt += char.lower()  # 大轉小
        else:
            changed_txt += char  # 數字不變
    return changed_txt


input_text = input("Enter the string: ")

if (check_valid(input_text)):  # 檢查輸入內容
    print(convert_to_result(input_text))
else:
    print("輸入錯誤")
