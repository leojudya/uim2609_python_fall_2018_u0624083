import uim2609_2018
import W9A1
import copy
import os
import shutil
import importlib


def main():
    tarpath = os.path.abspath("../2019-01-02_181439/")
    workpath = os.path.abspath(os.getcwd())
    os.chdir(tarpath)
    tarpath = os.path.abspath(tarpath) + '/'
    dirlist = os.listdir(os.getcwd())
    stuid = []
    for name in dirlist:
        yourfolder = name.split('_')[4].split('-')[0]
        try:
            wpath = tarpath + name + "/week_17/assignment"
            os.chdir(wpath)
            if os.path.exists('W17A0.py'):
                copypath = os.path.join(workpath, yourfolder)
                shutil.copyfile('W17A0.py', copypath + '.py')
        except Exception as e:
            pass
    os.chdir(workpath)
    stulist = os.listdir(os.getcwd())
    stulist.remove('W9A1.py')
    stulist.remove('W17A1.py')
    stulist.remove('uim2609_2018.py')
    stulist.remove('__pycache__')
    os.chdir('..')
    shutil.copyfile('W17A0.py', 'work/u0624083.py')

    for sid in stulist:
        stumod = None
        try:
            stumod = importlib.import_module(sid.split('.')[0], sid)
        except:
            continue
        # print(dir(stumod))
        if 'Mapcheck' in dir(stumod):
            print(sid.split('.')[0])
        if stumod is not None:
            del stumod


if __name__ == "__main__":
    main()
