import uim2609_2018
import sys
import importlib
import os
import copy
import W9A1


def main():
    x = int(sys.argv[1])
    y = int(sys.argv[2])
    rd = float(sys.argv[3])
    mark = sys.argv[4]
    symbol = sys.argv[5]

    mine = importlib.import_module('u0624083')
    youguyslist = os.listdir(os.getcwd())
    youguyslist.remove('W9A1.py')
    youguyslist.remove('W17A1.py')
    youguyslist.remove('uim2609_2018.py')
    youguyslist.remove('__pycache__')
    
    for one in youguyslist:
        stumod = None
        try:
            stumod = importlib.import_module(one.split('.')[0], one)
        except:
            continue

        if 'Mapcheck' in dir(stumod):
            map1 = W9A1.BuildMap(x, y, rd, mark, symbol)
            map2 = copy.deepcopy(map1)
            try:
                r1 = mine.Mapcheck(map1, x, y, rd, mark, symbol)
                if type(stumod.Mapcheck(map2, x, y, rd, mark, symbol)) is bool:
                    r2 = stumod.Mapcheck(map2, x, y, rd, mark, symbol)
                    if r1 == r2:
                        print(one.split('.')[0])
            except Exception as e:
                pass
        if stumod is not None:
            del stumod


if __name__ == "__main__":
    main()
