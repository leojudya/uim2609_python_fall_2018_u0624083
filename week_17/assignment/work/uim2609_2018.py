# Version: 20180925.0700
import builtins as __builtin__
import logging
import logging.handlers
import socket, sys, time
import uuid
import platform, subprocess

#server_address = ('10.10.5.174', 514)
serverSplunk_address = ('163.18.26.108', 9487)
serverUDP_address = ('nkustbcc.ddns.net', 9487)

def get_processor_info():
    if platform.system() == "Windows":
        return platform.processor()
    elif platform.system() == "Darwin":
        return subprocess.check_output(['/usr/sbin/sysctl', "-n", "machdep.cpu.brand_string"]).strip()
    elif platform.system() == "Linux":
        command = "cat /proc/cpuinfo"
        return subprocess.check_output(command, shell=True).strip()
    return ""


uim2609libID = '20180924.0700'
studentID = '00000000'

hostName = platform.node()
systemInfo = platform.system() + ';' + platform.release() + ';' + \
    platform.machine() + ';' + str(get_processor_info())
pythonVersion = platform.python_version()

extra = {'student_ID': studentID, 'hostName': hostName,
         'pythonVersion': pythonVersion, 'uim2609libID': uim2609libID, 'macAddr': str(hex(uuid.getnode())), 'systemInfo': systemInfo}



logger = logging.getLogger('UIM2609_2018')
logger.setLevel(logging.INFO)

# log to Splunk
splunkHandler = logging.handlers.SysLogHandler(address=('163.18.26.108', 9487), facility=19)
splunkHandler.setLevel(logging.INFO)

# add handler to the logger
logger.addHandler(splunkHandler)

formatter = logging.Formatter('Python: {"loggerName": "%(name)s", "studentID": "%(student_ID)s", "asciTime": "%(asctime)s", "pathName": "%(pathname)s", "logRecordCreationTime": "%(created)f", "functionName": "%(funcName)s", "levelNo": "%(levelno)s", "lineNo": "%(lineno)d", "time": "%(msecs)d", "levelName": "%(levelname)s", "hostID": "%(hostID)s", "macAddr":"%(macAddr)s", "systemInfo": "%(systemInfo)s", "pythonVersion": "%(pythonVersion)s", "uim2609libID": "%(uim2609libID)s", "message": "%(message)s" }')
splunkHandler.formatter = formatter


def infoToSplunk():
    #print(extra['student_ID'])
    try:
        import psutil
        extra.update({'cpu_times': str(psutil.cpu_times()), 'boot_time': str(psutil.boot_time())})
        
    except:
        pass
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    sock.settimeout(5)
    
    sent = sock.sendto(str(extra).encode(), serverSplunk_address)
    sock.close()



def print(*args, **kwargs):
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    sock.settimeout(5)

    
    for v in args:
        message = {'student_ID':  extra['student_ID'] ,  'messageInfo': str(v)}
        sent = sock.sendto(str(message).encode(), serverUDP_address)

    sock.close()
    return __builtin__.print(*args, **kwargs)
