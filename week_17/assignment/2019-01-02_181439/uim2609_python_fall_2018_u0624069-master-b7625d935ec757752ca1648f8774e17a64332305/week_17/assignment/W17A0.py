import uim2609_2018
import sys
import W9A1
import numpy as np
uim2609_2018.extra['student_ID'] = '0624069'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print


# a = start[0]   # 存放 x座標，直
# b = start[1]   # 存放 y座標，橫
# listTmp = [start]
# list = [start]


def checkRd():     # 檢查障礙物數量
    ans = True
    rightCount = 0
    checkCount = 0

    rightCount = int(x * y * rd)
    if rightCount / (x * y) < rd:
        rightCount += 1         # 正確數量

    for i in range(len(All_list)):
        for j in range(len(All_list[i])):
            if All_list[i][j] == "★":
                checkCount += 1

    if checkCount == rightCount:
        ans = True
    else:
        ans = False

    return ans


def direction(list, x, y, All_list):   # 方向   二維     list = [1, 0]
    # count = 0    # 計算上下左右哪裡有路，供start變換用
    a = list[0]   # a = 1
    b = list[1]   # b = 0
    listTmp = []

    if a > 0 and All_list[a - 1][b] != "★":  # 上
        listTmp.append([a - 1, b])   # 將有路的座標放入暫時陣列，再變成星星
        All_list[a - 1][b] = "★"
        # count += 1

    if b < y - 1 and All_list[a][b + 1] != "★":   # 右
        listTmp.append([a, b + 1])   # 將有障礙物的座標放入暫時陣列
        All_list[a][b + 1] = "★"
        # count += 1

    if a < x - 1 and All_list[a + 1][b] != "★":   # 下
        listTmp.append([a + 1, b])   # 將有障礙物的座標放入暫時陣列
        All_list[a + 1][b] = "★"
        # count += 1

    if b > 0 and All_list[a][b - 1] != "★":   # 左
        listTmp.append([a, b - 1])   # 將有障礙物的座標放入暫時陣列
        All_list[a][b - 1] = "★"
        # count += 1
    return listTmp


def Mapcheck(listTmp, x, y, rd, mark, symbol):   # 檢查有無被切割   # Templist
    ans = checkRd()
    start = [0, 0]
    counter = 0

    direction(start, x, y, All_list)   # [1, 0]
    counter += 1

    while counter < len(listTmp):
        start = listTmp[counter]
        direction(start)   # [1, 0]
        counter += 1

    for i in range(x):
        for j in range(y):
            if All_list[i][j] != "★":
                ans = False

    print(ans)   # True，False


if __name__ == '__main__':

    x = int(sys.argv[1])
    y = int(sys.argv[2])
    rd = float(sys.argv[3])
    mark = sys.argv[4]
    symbol = sys.argv[5]
    Tmplist = W9A1.BuildMap(x, y, rd, mark, symbol)
    All_list = Tmplist
    print(np.array(All_list))

    if ans:    # True
        Mapcheck(Tmplist, x, y, rd, mark, symbol)
