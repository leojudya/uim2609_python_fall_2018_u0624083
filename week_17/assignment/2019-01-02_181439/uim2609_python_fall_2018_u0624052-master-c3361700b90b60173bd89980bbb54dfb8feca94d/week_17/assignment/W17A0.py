# _ * _ coding: utf - 8 _ * _
# Python 3 version
import uim2609_2018
import sys
import W9A1
uim2609_2018.extra['student_ID'] = '0624052'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
# ***


def CheckMap(listTmp, x, y, rd, symbol, mark):
    itemnum = 0
    for i in range(0, len(listTmp)):
        for j in range(0, len(listTmp[i])):
            if(listTmp[i][j] == symbol):
                itemnum += 1
    if((itemnum / (x * y)) < rd):
        result = False
    else:
        result = True
    return result


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    if(CheckMap(Tmplist, x, y, rd, symbol, mark)):

        l = [[-1 for i in range(y)]for j in range(x)]
        n = 1
        # 建立不同的樹
        for i in range(x):
            for j in range(y):
                l[i][j] = n
                n += 1

        # printMap
        '''for i in range(len(Tmplist)):
            print()
            for j in range(len(Tmplist[i])):
                print(Tmplist[i][j], end=' ')'''

        for i in range(len(l)):
            for j in range(len(l[i])):
                if(Tmplist[i][j] == mark):
                    tmp = 0
                    ori = 0
                    # 向上檢查
                    if(i > 0):
                        if(Tmplist[i - 1][j] == mark):
                            if(l[i][j] > l[i - 1][j]):
                                ori = l[i][j]    # 記錄即將被替換的樹根
                                l[i][j] = l[i - 1][j]
                                tmp = l[i - 1][j]    # 紀錄新的樹根
                            else:
                                ori = l[i - 1][j]
                                l[i - 1][j] = l[i][j]
                                tmp = l[i][j]
                            for x in range(len(l)):
                                for y in range(len(l[x])):
                                    # 依序尋找所有相同的樹根，將其併入新樹根
                                    if(l[x][y] == ori):
                                        l[x][y] = tmp
                    # 向右檢查
                    if(j < len(l[i]) - 1):
                        if(Tmplist[i][j + 1] == mark):
                            if(l[i][j] > l[i][j + 1]):
                                ori = l[i][j]
                                l[i][j] = l[i][j + 1]
                                tmp = l[i][j + 1]
                            else:
                                orit = l[i][j + 1]
                                l[i][j + 1] = l[i][j]
                                tmp = l[i][j]
                            for x in range(len(l)):
                                for y in range(len(l[x])):
                                    if(l[x][y] == ori):
                                        l[x][y] = tmp

                    # 向左檢查
                    if(j > 0):
                        if(Tmplist[i][j - 1] == mark):
                            if(l[i][j] > l[i][j - 1]):
                                ori = l[i][j]
                                l[i][j] = l[i][j - 1]
                                tmp = l[i][j - 1]
                            else:
                                ori = l[i][j - 1]
                                l[i][j - 1] = l[i][j]
                                tmp = l[i][j]
                            for x in range(len(l)):
                                for y in range(len(l[x])):
                                    if(l[x][y] == ori):
                                        l[x][y] = tmp
                    # 向下檢查
                    if(i < len(l) - 1):
                        if(l[i - 1][j] == mark):
                            if(l[i][j] > l[i - 1][j]):
                                ori = l[i][j]
                                l[i][j] = l[i - 1][j]
                                tmp = l[i - 1][j]
                            else:
                                ori = l[i - 1][j]
                                l[i - 1][j] = l[i][j]
                                tmp = l[i][j]
                            for x in range(len(l)):
                                for y in range(len(l[x])):
                                    if(l[x][y] == ori):
                                        l[x][y] = tmp

        marknum = 0
        lnum = 0

        '''for i in range(len(l)):
            print()
            for j in range(len(l[i])):
                print(l[i][j], end=' ')'''

        # 紀錄起始點(樹根)
        root = 1
        for i in range(len(Tmplist)):
            for j in range(len(Tmplist[i])):
                if(Tmplist[i][j] == mark):
                    root = l[i][j]

        # 計算走訪節點數量與MARK是否相符
        for i in range(len(Tmplist)):
            for j in range(len(Tmplist[i])):
                if(Tmplist[i][j] == mark):
                    marknum += 1
                if(l[i][j] == root):
                    lnum += 1

        if(marknum == lnum):    # 若走訪的節點並非跟隨第一個ROOT，必定有密室
            return True
        else:
            return False

    else:
        return False


if __name__ == '__main__':
    # ll = [['☐', '☐', '☐', '★'], ['★', '★', '☐', '☐'], ['☐', '☐', '☐', '☐']]
    y = int(sys.argv[1])
    x = int(sys.argv[2])
    rd = float(sys.argv[3])
    mark = sys.argv[4]
    symbol = sys.argv[5]
    a = Mapcheck(W9A1.BuildMap(x, y, rd, mark, symbol), x, y, rd, mark, symbol)
    # a = Mapcheck(ll, x, y, rd, mark, symbol)
    print(a)
