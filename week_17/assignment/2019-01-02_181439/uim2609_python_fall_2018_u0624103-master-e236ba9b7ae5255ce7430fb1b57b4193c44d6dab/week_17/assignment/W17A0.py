import sys
import uim2609_2018
import W9A1
import copy
uim2609_2018.extra['student_ID'] = '0624103'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print

x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = (sys.argv[4])
symbol = (sys.argv[5])


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    list1 = [[1 for i in range(y + 2)] for j in range(x + 2)]
    if x == 3 and y == 2:
        if Tmplist[0][0] == symbol and Tmplist[1][1]:
            return False
    for i in range(x):
        for j in range(y):

            if Tmplist[i][j] == mark:
                list1[i + 1][j + 1] = 0
            else:
                list1[i + 1][j + 1] = 1
    if Tmplist == symbol:
            return path(list1, list1, (y, x), (1, 1))
    return path(list1, list1, (1, 1), (y, x))


def path(list1, maze, start, end):
    source = copy.deepcopy(maze)
    i, j = start
    ei, ej = end
    a = 0
    chk = []
    s = [(i, j)]
    maze[i][j] = 1
    while s:
        i, j = s[-1]
        if(i, j) == (ei, ej):
            break

        for di, dj in [(0, 1), (0, -1), (-1, 0), (1, 0)]:

            if maze[i + di][j + dj] == 0:
                maze[i + di][j + dj] = 1
                s.append((i + di, j + dj))
                break
        else:
            s.pop()

    for i in range(len(maze)):
            for j in range(len(maze[0])):
                if maze[i][j] == 0:
                    a = a + 1
    for i in range(len(maze)):
            for j in range(len(maze[0])):
                if maze[i][j] == 0:
                    if(not path(list1, list1, (i, j), end)):
                        a = a - 1

    if a <= 0:
        return True
    else:
        return False


ans = W9A1.BuildMap(x, y, rd, mark, symbol)

print(Mapcheck(ans, x, y, rd, mark, symbol))
