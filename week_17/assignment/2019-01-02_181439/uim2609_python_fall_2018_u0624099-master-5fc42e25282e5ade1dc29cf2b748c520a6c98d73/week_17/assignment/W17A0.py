import sys
import W9A1
import uim2609_2018
uim2609_2018.extra['student_ID'] = '0624099'
uim2609_2018.infoToSplunk()
print = uim2609_2018.print


def dfs(listTmp, i, j, x, y, mark, symbol):
    count = 1
    listTmp[i][j] = symbol
    if (i > 0 and listTmp[i-1][j] == mark):
        count = count + dfs(listTmp, i-1, j, x, y, mark, symbol)
    if (i < x-1 and listTmp[i+1][j] == mark):
        count = count + dfs(listTmp, i+1, j, x, y, mark, symbol)
    if (j > 0 and listTmp[i][j-1] == mark):
        count = count + dfs(listTmp, i, j-1, x, y, mark, symbol)
    if (j < y-1 and listTmp[i][j+1] == mark):
        count = count + dfs(listTmp, i, j+1, x, y, mark, symbol)
    return count


def Mapcheck(listTmp, x, y, rd, mark, symbol):
    a = 0
    i = 0
    j = 0
    for k in range(len(listTmp)):
        for m in range(len(listTmp[k])):
            if listTmp[k][m] == mark:
                a = a+1
                i = k
                j = m
    if a/(x*y) >= rd and a == dfs(listTmp, i, j, x, y, mark, symbol):
        result = not False
    else:
        result = False
    return result

if __name__ == '__main__':

    x = int(sys.argv[1])
    y = int(sys.argv[2])
    rd = float(sys.argv[3])
    mark = sys.argv[4]
    symbol = sys.argv[5]

    print(
        Mapcheck(W9A1.BuildMap(x, y, rd, mark, symbol), x, y, rd, mark, symbol)
    )
