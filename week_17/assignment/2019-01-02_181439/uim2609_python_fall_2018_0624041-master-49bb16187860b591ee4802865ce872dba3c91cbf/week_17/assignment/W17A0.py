import uim2609_2018
uim2609_2018.extra['student_ID'] = '0624041'
uim2609_2018.infoToSplunk()
print = uim2609_2018.print

import W9A1
import sys


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    global maze
    i, j = 0, 0
    pic = 1
    while pic != 1:
        if maze[i][j] == mark:
            pic = 2
            j -= 1
        i += 1
        j += 1
    run(i, j)
    a = True
    for i in range(x):
        for j in range(y):
            if maze[i][j] == mark:
                a = False
    print(a)


def run(x, y):
    global maze
    global mark
    global symbol
    t = [0, 1, 0, -1]
    t1 = [1, 0, -1, 0]
    maze[x][y] = symbol
    for i in range(0, 3):
        a = 0
        try:
            if(maze[x+t[i]][y+t1[i]] != symbol) and (x+t[i] >= 0) and (y+t1[i] >= 0):
                run(x+t[i], y+t1[i])
        except IndexError:
            a += 1
x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]
listA = []
maze = W9A1.BuildMap(x, y, rd, mark, symbol)
Mapcheck(maze, x, y, rd, mark, symbol)
