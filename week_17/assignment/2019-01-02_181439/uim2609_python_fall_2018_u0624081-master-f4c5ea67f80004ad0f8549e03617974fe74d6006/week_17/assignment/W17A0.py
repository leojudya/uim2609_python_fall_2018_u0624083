import numpy as np
import uim2609_2018
import W9A1
import sys
uim2609_2018.extra['student_ID'] = '0624081'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print


def change(maze, a, b, loc, mark, symbol):
    x = loc[0]
    y = loc[1]
    maptmp = maze
    maptmp[x][y] = symbol
    if x - 1 >= 0 and maptmp[x - 1][y] == mark:        # up
        maptmp = change(maptmp, a, b, [x - 1, y], mark, symbol)
    if x + 1 < a and maptmp[x + 1][y] == mark:       # down
        maptmp = change(maptmp, a, b, [x + 1, y], mark, symbol)
    if y - 1 >= 0 and maptmp[x][y - 1] == mark:        # left
        maptmp = change(maptmp, a, b, [x, y - 1], mark, symbol)
    if y + 1 < b and maptmp[x][y + 1] == mark:    # right
        maptmp = change(maptmp, a, b, [x, y + 1], mark, symbol)
    return maptmp


def Mapcheck(maze, x, y, rd, mark, symbol):
    print(np.array(maze))
    loc = [None] * 2
    for i in range(len(maze)):
        for j in range(len(maze[i])):
            if maze[i][j] == mark:
                loc[0], loc[1] = i, j
    maze = change(maze, x , y, loc, mark, symbol)
    count = 0
    for i in maze:
        for j in i:
            if j == mark:
                count += 1
    print('False' if count != 0 else 'True')


if __name__ == '__main__':
    row = int(sys.argv[1])
    column = int(sys.argv[2])
    rd = float(sys.argv[3])
    mark = sys.argv[4]
    symbol = sys.argv[5]
    maze = W9A1.BuildMap(row, column, rd, mark, symbol)
    print(np.array(maze))
    Mapcheck(maze, row, column, mark)

