# _ * _ coding: utf - 8 _ * _
# Python 3 version
import uim2609_2018
import sys
import W9A1
uim2609_2018.extra['student_ID'] = '0624031'
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
# ***


def Mapcheck(Tmplist, x, y, mark, symbol):
    for i in range(len(Tmplist)):
        for j in range(len(Tmplist[i])):
            print(Tmplist[i][j], end=' ')
            if Tmplist[i][j] == mark:
                row = i
                col = j
        print()
    change(Tmplist, row, col, mark)
    count = 0
    for i in Tmplist:
        for j in i:
            if j == mark:
                count += 1
    print('False' if count != 0 else 'True')


def change(Tmplist, x, y, mark):
    Tmplist[x][y] = '*'
    if x-1 >= 0 and Tmplist[x-1][y] == mark:
        change(Tmplist, x-1, y, mark)
    if x+1 < len(Tmplist)-1 and Tmplist[x+1][y] == mark:
        change(Tmplist, x+1, y, mark)
    if y-1 >= 0 and Tmplist[x][y-1] == mark:
        change(Tmplist, x, y-1, mark)
    if y+1 < len(Tmplist)-1 and Tmplist[x][y+1] == mark:
        change(Tmplist, x, y+1, mark)
    return Tmplist


x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]
if __name__ == '__main__':
    Tmplist = W9A1.BuildMap(x, y, rd, mark, symbol)
    Mapcheck(Tmplist, x, y, mark, symbol)
