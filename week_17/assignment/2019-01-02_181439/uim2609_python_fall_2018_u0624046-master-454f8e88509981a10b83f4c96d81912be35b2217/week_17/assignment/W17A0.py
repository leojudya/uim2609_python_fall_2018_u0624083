import sys
import W9A1
import random
import math
# _ * _ coding: utf - 8 _ * _
# Python 3 version
import uim2609_2018
uim2609_2018.extra['student_ID'] = '0624046'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print
# ***


def Mapcheck(map, x, y, rd, mark, symbol):
    ans = False
    num = 0
    count = 0
    pic = [[symbol] * (y+2) for i in range(x+2)]
    # 邊界
    for i in range(x):
        for j in range(y):
            pic[i+1][j+1] = map[i][j]
    # 設數值
    for i in range(x):
        for j in range(y):
            if (pic[i+1][j+1] == mark):
                num += 1
                pic[i+1][j+1] = num
    for i in range(1, x+1):
        for j in range(1, y+1):
            if(pic[i][j] == symbol):
                continue
            if(pic[i][j+1] != symbol):
                if (pic[i][j] < pic[i][j+1]):
                    num1 = pic[i][j+1]
                    num2 = pic[i][j]
                    for a in range(1, x+1):
                        for b in range(1, y+1):
                            if(pic[a][b] == symbol):
                                continue
                            if(pic[a][b] == num1):
                                pic[a][b] = num2
                # 右
            if(pic[i][j-1] != symbol):
                if (pic[i][j] < pic[i][j-1]):
                    num1 = pic[i][j-1]
                    num2 = pic[i][j]
                    for a in range(1, x+1):
                        for b in range(1, y+1):
                            if(pic[a][b] == symbol):
                                continue
                            if(pic[a][b] == num1):
                                pic[a][b] = num2
                # 左
            if(pic[i+1][j] != symbol):
                if (pic[i][j] < pic[i+1][j]):
                    num1 = pic[i+1][j]
                    num2 = pic[i][j]
                    for a in range(1, x+1):
                        for b in range(1, y+1):
                            if(pic[a][b] == symbol):
                                continue
                            if(pic[a][b] == num1):
                                pic[a][b] = num2
                # 上
            if(pic[i-1][j] != symbol):
                if (pic[i][j] < pic[i-1][j]):
                    num1 = pic[i-1][j]
                    num2 = pic[i][j]
                    for a in range(1, x+1):
                        for b in range(1, y+1):
                            if(pic[a][b] == symbol):
                                continue
                            if(pic[a][b] == num1):
                                pic[a][b] = num2
                # 下
    for i in range(1, x+1):
        for j in range(1, y+1):
            if(pic[i][j] == symbol):
                continue
            count += pic[i][j]
    if (num == count):
        ans = True
    '''for i in range(len(pic)):
        print()
        for j in range(len(pic[0])):
            print(pic[i][j], end="\t")'''
    return ans


if __name__ == '__main__':
    x = int(sys.argv[1])
    y = int(sys.argv[2])
    rd = float(sys.argv[3])
    mark = sys.argv[4]
    symbol = sys.argv[5]
    print(Mapcheck(W9A1.BuildMap(
        x, y, rd, mark, symbol), x, y, rd, mark, symbol))
