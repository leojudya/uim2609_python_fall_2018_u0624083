import numpy as np
import sys
import W9A1
import uim2609_2018
uim2609_2018.extra['student_ID'] = '0624029'  # 換成你的學號
uim2609_2018.infoToSplunk()
print = uim2609_2018.print

x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]


def Mapcheck(tmplist, x, y, rd, mark,symbol):
    symbol = [None] * 2
    for i in range(len(tmplist)):
        for j in range(len(tmplist[i])):
            if tmplist[i][j] == mark:
                symbol[0], symbol[1] = i, j
    tmplist = change(symbol, mark)
    count = 0
    for i in tmplist:
        for j in i:
            if j == mark:
                count += 1
    print('False' if count != 0 else 'True')


def change(symbol, mark):
    x = symbol[0]
    y = symbol[1]
    tmplist[x][y] = '*'
    if x - 1 >= 0 and tmplist[x - 1][y] == mark:   # up
        change([symbol[0] - 1, symbol[1]], mark)
    if x + 1 < x and tmplist[x + 1][y] == mark:    # down
        change([symbol[0] + 1, symbol[1]], mark)
    if y - 1 >= 0 and tmplist[x][y - 1] == mark:   # left
        change([symbol[0], symbol[1] - 1], mark)
    if y + 1 < y and tmplist[x][y + 1] == mark:    # right
        change([symbol[0], symbol[1] + 1], mark)
    return tmplist

if __name__ == '__main__':
    tmplist = W9A1.BuildMap(x, y, rd, mark, symbol)
    print(np.array(tmplist))
    Mapcheck(tmplist, x, y, rd, mark,symbol)
