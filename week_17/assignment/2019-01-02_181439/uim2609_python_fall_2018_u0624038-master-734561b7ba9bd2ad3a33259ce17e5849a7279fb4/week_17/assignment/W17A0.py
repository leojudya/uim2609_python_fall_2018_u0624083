import uim2609_2018
import sys
import random
import W9A1
uim2609_2018.extra['student_ID'] = '0624038'  # 換成你的學號
uim2609_2018.infoToSplunk()

dirs = [lambda x, y: (x + 1, y),
        lambda x, y: (x - 1, y),
        lambda x, y: (x, y - 1),
        lambda x, y: (x, y + 1)]


def Mapcheck(x2, y2,mark,symbol,maze):
    x1 = 0
    y1 = 0
    x2 = x2-1
    y2 = y2-1
    stack = []
    stack.append((x1, y1))
    while len(stack) > 0:
        curNode = stack[-1]
        maze[curNode[0]][curNode[1]] = symbol
        if curNode[0] == x2 and curNode[1] == y2:
            for dir in dirs:
                nextNode = dir(curNode[0], curNode[1])
                if nextNode[0] < x and nextNode[1] < y and\
                   nextNode[0] >= 0 and nextNode[1] >= 0:
                    if maze[nextNode[0]][nextNode[1]] == mark:
                        stack.append(nextNode)
                        maze[nextNode[0]][nextNode[1]] = symbol
                        break
        else:
            maze[curNode[0]][curNode[1]] = symbol
            stack.pop()
    ans = True
    for i in range(0, x):
        for j in range(0, y):
            if maze[i][j] == mark:
                ans = False
                break
    print(ans)
    



if __name__ == '__main__':
    x = int(sys.argv[1])
    y = int(sys.argv[2])
    rd = float(sys.argv[3])
    mark = sys.argv[4]
    symbol = sys.argv[5]
    #maze = W9A1.BuildMap(x, y, rd, mark, symbol)
    #for i in range(0, x):
    #    for j in range(0, y):
    #       if j == y-1:
    #            print(maze[i][j])
    #        else:
    #            print(maze[i][j], end=' ')
    Mapcheck(x,y,mark,symbol,(W9A1.BuildMap(x, y, rd, mark, symbol))