
# coding: utf-8

# In[47]:
import uim2609_2018.py
import W9A1.py
import sys


def Mapcheck(Tmplist, x, y, rd, mark, symbol):
    p = 0
    q = 0
    for i in range(x):
        for j in range(y):
            if(Tmplist[q][p] == mark):
                Tmplist[q][p] = 'A'
                break
            p = p+1
        if(Tmplist[q][p] == 'A'):
            break
        q = q+1
        p = 0
    a = p
    b = q
    for k in range(x*y):
        for i in range(x):
            for j in range(y):
                if(Tmplist[b][a] == 'A'):
                    if(a == 0 and b == 0):
                        if(Tmplist[b][a+1] == mark):
                            Tmplist[b][a+1] = 'A'
                        if(Tmplist[b+1][a] == mark):
                            Tmplist[b+1][a] = 'A'
                    elif(a == 0 and b == x-1):
                        if(Tmplist[b-1][a] == mark):
                            Tmplist[b-1][a] = 'A'
                        if(Tmplist[b][a+1] == mark):
                            Tmplist[b][a+1] = 'A'
                    elif(a == y-1 and b == 0):
                        if(Tmplist[b][a-1] == mark):
                            Tmplist[b][a-1] = 'A'
                        if(Tmplist[b-1][a] == mark):
                            Tmplist[b-1][a] = 'A'
                    elif(a == y-1 and b == x-1):
                        if(Tmplist[b-1][a] == mark):
                            Tmplist[b-1][a] = 'A'
                        if(Tmplist[b][a-1] == mark):
                            Tmplist[b][a-1] = 'A'
                    elif(a == 0):
                        if(Tmplist[b-1][a] == mark):
                            Tmplist[b-1][a] = 'A'
                        if(Tmplist[b][a-1] == mark):
                            Tmplist[b][a-1] = 'A'
                        if(Tmplist[b+1][a] == mark):
                            Tmplist[b+1][a] = 'A'
                    elif(b == 0):
                        if(Tmplist[b][a-1] == mark):
                            Tmplist[b][a-1] = 'A'
                        if(Tmplist[b+1][a] == mark):
                            Tmplist[b+1][a] = 'A'
                        if(Tmplist[b][a+1] == mark):
                            Tmplist[b][a+1] = 'A'
                    elif(a == y-1):
                        if(Tmplist[b-1][a] == mark):
                            Tmplist[b-1][a] = 'A'
                        if(Tmplist[b][a-1] == mark):
                            Tmplist[b][a-1] = 'A'
                        if(Tmplist[b+1][a] == mark):
                            Tmplist[b+1][a] = 'A'
                    elif(b == x-1):
                        if(Tmplist[b-1][a] == mark):
                            Tmplist[b-1][a] = 'A'
                        if(Tmplist[b][a+1] == mark):
                            Tmplist[b][a+1] = 'A'
                        if(Tmplist[b][a-1] == mark):
                            Tmplist[b][a-1] = 'A'
                    else:
                        if(Tmplist[b][a+1] == mark):
                            Tmplist[b][a+1] = 'A'
                        if(Tmplist[b][a-1] == mark):
                            Tmplist[b][a-1] = 'A'
                        if(Tmplist[b+1][a] == mark):
                            Tmplist[b+1][a] = 'A'
                        if(Tmplist[b-1][a] == mark):
                            Tmplist[b-1][a] = 'A'
                a = a+1
                if(a == y):
                    break
            b = b+1
            a = 0
            if(b == x):
                break
        a = p
        b = q
    for i in range(x):
        for j in range(y):
            if(Tmplist[i][j] == mark):
                return False
    return True
list = input().split()
x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]
print(Mapcheck(W9A1.BuildMap(x, y, rd, mark, symbol), x, y, rd, mark, symbol))
