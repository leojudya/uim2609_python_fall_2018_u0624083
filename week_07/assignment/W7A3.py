import uim2609_2018


def quick_sort(qlist):
    less = []
    more = []
    if not qlist:
        return []
    else:
        pivot = qlist[0]
        for x in qlist:
            if (x < pivot):
                less.append(x)
        for x in qlist[1:]:
            if(x >= pivot):
                more.append(x)
        return quick_sort(more) + [pivot] + quick_sort(less)

num_list = [int(x) for x in input("請輸入數字： ").split()]
for i in quick_sort(num_list):
    print(i, end=' ')
print()
