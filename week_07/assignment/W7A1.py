import uim2609_2018
color_block = ['□', '■']
m = int(input('m: '))
n = int(input('n: '))
for x in range(n):
    for y in range(m):
        print(color_block[(x + y) % 2], end='')  # (x+y) mod 2 {==1->black 反之}
    print()
