import uim2609_2018
import sys
import random

x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
symbol = sys.argv[4]
sym_count = int(round(x * y * rd, 0))
blank_count = x * y - sym_count
sym_list = []
for i in range(sym_count):
    sym_list.append(symbol)
for i in range(blank_count):
    sym_list.append('☐')
sym_list = random.sample(sym_list, len(sym_list))
q = 0
for i in range(x):
    for j in range(y):
        print(sym_list[q], end='')
        q += 1
    print()
