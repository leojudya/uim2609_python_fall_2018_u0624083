import sys
import W9A1
import uim2609_2018
import math


def CheckMap(listTmp, x, y, rd, symbol, mark):
    counting_dict = {symbol: 0, mark: 0}
    for i in listTmp:
        for sym in i:
            if (sym == symbol):
                counting_dict[symbol] += 1
            else:
                counting_dict[mark] += 1
    sym_count = 0
    if (math.floor((x*y*rd)) / (x*y) >= rd):
        sym_count = math.floor((x*y*rd))
    else:
        sym_count = math.ceil((x*y*rd))
    if (sym_count == counting_dict[symbol] and
            x*y-sym_count == counting_dict[mark]):
        return True
    return False


x = int(sys.argv[1])
y = int(sys.argv[2])
rd = float(sys.argv[3])
mark = sys.argv[4]
symbol = sys.argv[5]

if __name__ == '__main__':
    print(CheckMap(W9A1.BuildMap(x, y, rd, mark, symbol),
                   x, y, rd, symbol, mark))
