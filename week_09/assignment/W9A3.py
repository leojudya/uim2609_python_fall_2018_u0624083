import uim2609_2018
import sys


def fact(n):
    if n == 2:
        return 2
    else:
        return n*fact(n-1)


print(fact(int(sys.argv[1])))
